<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Background Video Youtube Responsivo</title>
	<link rel="stylesheet" type="text/css" href="css/youtube-bg.css"/>
</head>
<body>
	<div class="embed-container"> 
		<!-- VIDEO DE BACKGROUND --> 
		<iframe src="https://www.youtube.com/embed/kPDnw3_1GOI?%20rel=0&autoplay=1&controls=0&mute=1&showinfo=0&autohide=1&playlist=kPDnw3_1GOI&loop=1" frameborder="0"></iframe> 
		<!-- CONTEUDO ENCIMA DO VIDEO --> 
		<div class="conteudo flex-center"> 
			<h1 class="flexed">Background Video<span class="yt">Youtube</span>Responsivo</h1> 
			<br><br> 
			<a href="https://www.youtube.com/watch?v=kPDnw3_1GOI" class="btn toggle flex-center" style="text-decoration:none;" target="_blank">Video</a> 
		</div> 
	</div> 

</body>
</html>